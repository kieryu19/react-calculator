import './App.css';
import { useState, useEffect } from 'react';
import NumberFormat from "react-number-format";

function App() {

  const [prevState, setPrevState] = useState("");
  const [currState, setCurrState] = useState("");
  const [input, setInput] = useState("0");
  const [operator, setOperator] = useState(null);
  const [total, setTotal] = useState(false);

  const inputNum = (e) => {
    if(currState.includes(".") || e.target.innerText === ".") return;

    if(total) {
      setPrevState("");
    }

    currState ? setCurrState((pre) => pre + e.target.innerText) : setCurrState(e.target.innerText);
    setTotal(false);
  };

  useEffect(() => {
    setInput(currState);
  }, [currState])

  useEffect(() => {
    setInput("0");
  }, [])

  const operatorType = (e) => {
    setTotal(false);
    setOperator(e.target.innerText);
    if (currState === "") return;
    if(prevState !== "") {
      equals();
    } else {
      setPrevState(currState);
      setCurrState("");
    }
  };

  const equals = (e) => {
    if (e?.target.innerText === "=") {
      setTotal(true);
    }

    let cal;
    switch(operator) {
      case "/":
        cal = String(parseFloat(prevState) / parseFloat(currState));
        break;
      case "+":
        cal = String(parseFloat(prevState) + parseFloat(currState));
        break;
      case "X":
        cal = String(parseFloat(prevState) * parseFloat(currState));
        break;
      case "-":
        cal = String(parseFloat(prevState) - parseFloat(currState));
        break;
      default:
        return;
    }

    setInput("");
    setPrevState(cal);
    setCurrState("");

  };

  const clear = e => {
    setPrevState("");
    setCurrState("");
    setInput("0");
  };

  const percent = () => {
    prevState ? setCurrState(String(parseFloat( currState ) / 100 * prevState)) :
    setCurrState(String(parseFloat(currState) / 100));
  }

  const positiveNegative = () => {
    if(currState.charAt(0) === "-") {
      setCurrState(currState.subString(1));
    } else {
      setCurrState("-" + currState);
    }
  }


  return (
    <div className="container">
      <div className="wrapper">

        <div className="screen">
          {input !== ""  || input === "0" ? (
            <NumberFormat 
              value={input} 
              displayType={"text"} 
              thousandSeparator={true} /> ) : ( 
            <NumberFormat 
              value={prevState} 
              displayType={"text"} 
              thousandSeparator="true" /> 
          )}
        </div>
        <div className="btn light-gray" onClick={clear}>AC</div>
        <div className="btn light-gray" onClick={percent}>%</div>
        <div className="btn light-gray" onClick={positiveNegative}>+/-</div>
        <div className="btn orange" onClick={operatorType}>/</div>
        <div className="btn" onClick={inputNum}>7</div>
        <div className="btn" onClick={inputNum}>8</div>
        <div className="btn" onClick={inputNum}>9</div>
        <div className="btn orange" onClick={operatorType}>X</div>
        <div className="btn" onClick={inputNum}>4</div>
        <div className="btn" onClick={inputNum}>5</div>
        <div className="btn" onClick={inputNum}>6</div>
        <div className="btn orange" onClick={operatorType}>+</div>
        <div className="btn" onClick={inputNum}>1</div>
        <div className="btn" onClick={inputNum}>1</div>
        <div className="btn" onClick={inputNum}>3</div>
        <div className="btn orange" onClick={operatorType}>-</div>
        <div className="btn zero" onClick={inputNum}>0</div>
        <div className="btn" onClick={inputNum}>.</div>
        <div className="btn" onClick={equals}>=</div>

      </div>
    </div>
  );
}

export default App;
